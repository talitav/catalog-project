import os
from app import app

port = int(os.environ.get('PORT', 8080))
if __name__ == '__main__':
    app.run(threaded=True)
