from sqlalchemy import insert
from sqlalchemy import select
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Boolean, DateTime, String

from database.config import db_engine, Base
from database.config import metadata

Session = sessionmaker(bind=db_engine)


class Tags(Base):
    __tablename__ = 'tags'

    slug = Column(String, primary_key=True)
    name = Column(String)
    description = Column(String)
    active = Column(Boolean)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    @classmethod
    def tags_tb(cls):
        return metadata.tables['tags']

    @classmethod
    def insert_tag(cls, payload):
        conn = db_engine.connect()
        trans = conn.begin()
        try:
            insert_stmt = insert(cls.tags_tb())
            result = conn.execute(insert_stmt, payload)
        except Exception:
            trans.rollback()
            conn.close()
            raise ValueError('A very specific bad thing happened.')
        trans.commit()
        conn.close()

        return result

    @classmethod
    def get_tag_by_slug(cls, slug):
        conn = db_engine.connect()
        trans = conn.begin()
        query = select(
            ['*']
        ).select_from(
            cls.tags_tb()
        ).where(
            cls.tags_tb().c.slug == slug
        )
        result = conn.execute(query)
        row = result.fetchone()
        trans.commit()
        conn.close()

        return row

    @classmethod
    def get_tag_instance(cls, slug):
        session = Session()
        tag = session.query(cls).filter_by(slug=slug).first()

        session.commit()

        return tag
