from datetime import datetime
from dateutil import tz
from flask import request, jsonify
from marshmallow import ValidationError
from slugify import slugify

from .models import Tags
from .schemas import TagSchema


def create_tags():
    payload = request.get_json()

    # Validate payload
    try:
        schema = TagSchema()
        payload = schema.load(payload)
    except ValidationError as err:
        errors = {'fields': err.messages}
        return jsonify(errors), 400

    # Generate datetimes
    now = datetime.now(tz.UTC)
    created_at = now
    updated_at = now

    # Add extra fields
    tag_slug = slugify(payload.get('name'))
    payload['slug'] = tag_slug
    payload['active'] = True
    payload['created_at'] = created_at
    payload['updated_at'] = updated_at

    try:
        Tags.insert_tag(payload)
    except Exception:
        return jsonify({}), 500

    return jsonify({'slug': tag_slug}), 201


def read_tags(tag_slug):
    row = Tags.get_tag_by_slug(tag_slug)

    if not row:
        return jsonify({}), 404

    tag = dict(row)

    return jsonify(tag), 200


def update_tags(tag_slug):
    return jsonify({}), 200


def delete_tags(tag_slug):
    return jsonify({}), 200
