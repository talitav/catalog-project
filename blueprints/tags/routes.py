from flask import Blueprint

from .controllers import create_tags, read_tags, update_tags, delete_tags


tags_bp_v1 = Blueprint('tags', __name__, url_prefix='/v1/tags')

tags_bp_v1.route('/', methods=['POST'])(create_tags)
tags_bp_v1.route('/<tag_slug>', methods=['GET'])(read_tags)
tags_bp_v1.route('/<tag_slug>', methods=['PUT'])(update_tags)
tags_bp_v1.route('/<tag_slug>', methods=['PUT'])(delete_tags)
