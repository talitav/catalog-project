from marshmallow import Schema, fields
from marshmallow import EXCLUDE
from marshmallow.decorators import post_dump


class TagSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    name = fields.String(required=True)

    description = fields.String(
        allow_none=True,
        missing=None)


class TagsRetrieve(Schema):
    slug = fields.String()

    @post_dump
    def slug_list(self, obj, many, *args, **kwargs):
        if many:
            return obj['slug']
        return obj
