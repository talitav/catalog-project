import re
from marshmallow import Schema, fields
from marshmallow import EXCLUDE
from marshmallow.decorators import post_load

from blueprints.tags.schemas import TagsRetrieve


class ProductSchema(Schema):
    code = fields.String(required=True)
    name = fields.String()
    description = fields.String(
        allow_none=True,
        missing=None
    )
    qty = fields.Integer(required=True)
    price = fields.Float(required=True)
    expiration_date = fields.Date()
    tags = fields.Nested(TagsRetrieve, many=True)

    class Meta:
        unknown = EXCLUDE


class ProductInput(ProductSchema):
    tags = fields.List(fields.String)

    @post_load
    def sanitize_string_fields(self, obj, *args, **kwargs):
        if obj.get('name'):
            obj['name'] = self._sanitize_name_field(obj['name'])

        if obj.get('code'):
            obj['code'] = self._sanitize_code_field(obj['code'])

        if(obj.get('description')):
            obj['description'] = self._sanitize_description_field(
                obj['description']
            )

        return obj

    def _removes_unnecessary_white_space(self, s):
        return re.sub('\s+', ' ', s.strip())

    def _sanitize_name_field(self, name):
        return self._removes_unnecessary_white_space(name).capitalize()

    def _sanitize_description_field(self, description):
        return self._removes_unnecessary_white_space(description)

    def _sanitize_code_field(self, code):
        return re.sub('\s*[^a-zA-Z0-9]', '', code)


class ProductUpdate(ProductInput):

    class Meta:
        exclude = ('code', )
