from flask import Blueprint

from .controllers import create_product, retrieve_product, update_product

products_bp_v1 = Blueprint('products', __name__, url_prefix='/v1/products')

products_bp_v1.route('/<product_code>', methods=['GET'])(retrieve_product)
products_bp_v1.route('/', methods=['POST'])(create_product)
products_bp_v1.route('/<product_code>', methods=['PUT'])(update_product)
