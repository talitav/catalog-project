from flask import request, jsonify
from marshmallow.exceptions import ValidationError
from blueprints.products.exceptions import (
    ProductNotFoundException,
    TagNotFoundException
)

from blueprints.products.models import Products
from blueprints.products.schemas import (
    ProductSchema, ProductInput, ProductUpdate
)


def retrieve_product(product_code):
    product_obj = Products.get_product_by_code(product_code)

    if not product_obj:
        return jsonify({}), 404

    product = ProductSchema().dump(product_obj)

    return jsonify(product), 200


def create_product():
    body = request.get_json()
    schema = ProductInput()
    try:
        payload = schema.load(body)
        Products.create(payload=payload)
    except TagNotFoundException as exc:
        return jsonify({
            'code': 'tag_not_found',
            'details': {
                'slug': exc.tag
            }
        }), 400
    except Exception:
        return jsonify({}), 500

    return jsonify({}), 201


def update_product(product_code):
    body = request.get_json()

    try:
        schema = ProductUpdate()
        payload = schema.load(body, partial=True)

        Products.update(product_code=product_code, payload=payload)
    except TagNotFoundException as exc:
        return jsonify({
            'error_code': 'tag_not_found',
            'details': {
                'slug': exc.tag
            }
        }), 404
    except ProductNotFoundException:
        return jsonify({
            'error_code': 'product_not_found',
        }), 404
    except ValidationError as exc:
        return jsonify({
            'error_code': 'validation_error',
            'details': str(exc)
        }), 400
    except Exception:
        return jsonify({}), 500

    return jsonify({}), 204
