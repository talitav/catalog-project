class TagNotFoundException(Exception):

    def __init__(self, tag):
        self.tag = tag


class ProductNotFoundException(Exception):
    pass
