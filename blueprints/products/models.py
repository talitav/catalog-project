from datetime import datetime
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import DateTime, Float, String, Date
from sqlalchemy.orm import sessionmaker
from blueprints.products.exceptions import (
    ProductNotFoundException,
    TagNotFoundException
)

from blueprints.tags.models import Tags

from database.config import db_engine, Base

Session = sessionmaker(bind=db_engine)


class ProductsTags(Base):
    __tablename__ = 'products_tags'

    id = Column(Integer, primary_key=True)
    tag_slug = Column(ForeignKey('tags.slug'))
    product_id = Column(ForeignKey('products.id'))


class Products(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    code = Column(String)
    name = Column(String)
    description = Column(String)
    qty = Column(Integer)
    price = Column(Float)
    expiration_date = Column(Date)
    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(
        DateTime,
        default=datetime.utcnow(),
        onupdate=datetime.utcnow()
    )
    tags = relationship(Tags, secondary='products_tags')

    @staticmethod
    def _create_tag_relationship(product, tags_payload):

        for tag_slug in tags_payload:
            tag = Tags.get_tag_instance(tag_slug)
            if not tag:
                raise TagNotFoundException(tag=tag_slug)
            product.tags.append(tag)
        return product

    @classmethod
    def get_product_by_code(cls, product_code):
        session = Session()
        product = session.query(Products).filter_by(code=product_code).first()

        return product

    @classmethod
    def create(cls, payload):
        tags_payload = payload['tags']
        session = Session()

        try:
            product = cls(
                code=payload.get('code'),
                name=payload.get('name'),
                description=payload.get('description'),
                qty=payload.get('qty'),
                price=payload.get('price'),
                expiration_date=payload.get('expiration_date'),
            )

            product = cls._create_tag_relationship(
                product=product,
                tags_payload=tags_payload
            )

            session.add(product)

            session.commit()
        except Exception:
            session.rollback()
            raise

        return True

    @classmethod
    def update(cls, product_code, payload):
        tags_payload = payload.get('tags')

        if tags_payload:
            del payload['tags']

        session = Session()

        try:
            product_q = session.query(Products).filter_by(
                code=product_code
            )

            product = product_q.first()

            if not product:
                raise ProductNotFoundException()

            product_q.update(payload)

            if tags_payload:
                product.tags.clear()

                product = cls._create_tag_relationship(
                    product=product,
                    tags_payload=tags_payload
                )

            session.add(product)
            session.commit()

        except Exception:
            session.rollback()
            raise

        return True
