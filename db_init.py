from database.config import db_engine, Base

meta = Base.metadata


from blueprints.tags.models import Tags
from blueprints.products.models import Products, ProductsTags

print('>> Inicializing database')

Tags.__table__.create(bind=db_engine, checkfirst=True)
Products.__table__.create(bind=db_engine, checkfirst=True)
ProductsTags.__table__.create(bind=db_engine, checkfirst=True)
