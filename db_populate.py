import csv
from distutils.util import strtobool

from sqlalchemy.orm.session import sessionmaker
from blueprints.tags.models import Tags
from database.config import db_engine, Base
from datetime import datetime, date
from dateutil import tz

from sqlalchemy import insert

from blueprints.products.models import Products

meta = Base.metadata


###############################################################################
print('>> Importing data to table tags')
tags_tb = meta.tables['tags']
with open("./tags_import.csv", "r") as f:
    csv_reader = csv.DictReader(f)

    now = datetime.now(tz.UTC)
    created_at = now
    updated_at = now

    payload = []
    for row in csv_reader:
        active = True
        if row['active'] != 'true':
            active = False

        description = row['description']
        if not row['description']:
            description = None

        tag = {
            'slug': row['slug'],
            'name': row['name'],
            'description': description,
            'active': active,
            'created_at': created_at,
            'updated_at': updated_at,
        }
        payload.append(tag)
        print('tag: {} added!'.format(row['slug']))

    conn = db_engine.connect()
    trans = conn.begin()
    try:
        insert_stmt = insert(tags_tb)
        result = conn.execute(insert_stmt, payload)
    except Exception as e:
        trans.rollback()
        conn.close()
        raise ValueError('A very specific bad thing happened.')
    trans.commit()
    conn.close()

# Import data to table products
################################################################################
print('>> Importing data to table products')
products_tb = meta.tables['products']
with open("./products_import.csv", "r") as f:
    csv_reader = csv.DictReader(f)

    now = datetime.now(tz.UTC)
    created_at = now
    updated_at = now

    payload = []
    for row in csv_reader:
        active = True
        if row['active'] != 'true':
            active = False

        description = row['description']
        if not row['description']:
            description = None

        product = {
            'code': row['code'],
            'name': row['name'],
            'qty': row['qty'],
            'price': row['price'],
            'expiration_date': date.fromisoformat(row['expiration_date']),
            'description': description,
            'active': active,
            'created_at': created_at,
            'updated_at': updated_at,
        }
        payload.append(product)
        print('product: {} added!'.format(row['name']))

    conn = db_engine.connect()
    trans = conn.begin()
    try:
        insert_stmt = insert(products_tb)
        result = conn.execute(insert_stmt, payload)
    except Exception as e:
        trans.rollback()
        conn.close()
        raise ValueError('A very specific bad thing happened.')
    trans.commit()
    conn.close()

print('Finished!')
