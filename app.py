from flask import Flask
from blueprints.products.routes import products_bp_v1
from blueprints.tags.routes import tags_bp_v1

app = Flask(__name__)
app.register_blueprint(tags_bp_v1)
app.register_blueprint(products_bp_v1)


if __name__ == '__main__':
    app.run(debug=True)
