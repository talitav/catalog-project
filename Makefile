

dependencies:
	pip install --upgrade pip
	pip install -r requirements.txt

lint:
	flake8 blueprints/

run:
	flask run

tests:
	pytest -x


docker-up:
	docker-compose up

docker-down:
	docker-compose down

db-populate:
	docker run catalog-backend python db_populate.py
