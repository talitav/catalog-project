# Backend Exercise (1)

## Environment

### Conda
* $ conda create -n env Python=3.7
* $ source activate env
* $ pip install --upgrade pip
* $ pip install -r requirements.txt

### Virtualenv
* $ mkdir venv
* $ python3.7 -m venv /path/to/venv
* $ source /venv/bin/activate
* $ pip install --upgrade pip
* $ pip install -r requirements.txt

## Banco de dados

Para criar o banco de dados, execute o comando:
* $ python db_init.py


## Execução do servidor

### Executar em modo desenvolvimento
* `make run`

### Executar com docker
* `make docker-up`
* `make db-populate` (Inserir registros no banco para testes)


## Endpoints


### POST v1/products/

```json
{
    "name": "        arroz ",
    "description": " Arroz agulhinha tipo 1 (5kg)         ",
    "code": " 014-342-134        ",
    "qty": 10,
    "price": 29.95,
    "expiration_date": "2022-09-03",
    "tags": ["arroz", "mercearia"]
}
```

#### Status code

* 201 - Produto criado com sucesso
* 400 - Erro de validação dos dados
* 500 - Erro interno no servidor

### GET v1/products/{product_code}

* Resposta
```json
{
    "name": "Arroz",
    "description": "Arroz agulhinha tipo 1 (5kg)",
    "code": "014342134",
    "qty": 10,
    "price": 29.95,
    "expiration_date": "2022-09-03",
    "tags": [
        {
            "slug": "arroz",
            "name": "Arroz",
            "description": null
        },
        {
            "slug": "mercearia",
            "name": "Mercearia",
            "description": "Itens de mercearia para cozinha"
       }
    ]
}
```

#### Status code

* 201 - Produto retornado com sucesso
* 404 - Produto não encontrado
* 500 - Erro interno no servidor


### PUT v1/products/{product_code}

```json
{
    "name": "        arroz ",
    "description": " Arroz agulhinha tipo 1 (5kg)         ",
    "code": " 014-342-134        ",
    "qty": 10,
    "price": 29.95,
    "expiration_date": "2022-09-03",
    "tags": ["arroz", "mercearia"]
}
```

#### Status code

* 204 - Atualizado com sucesso
* 400 - Erro de validação dos dados
* 404 - Produto/Tag não encontrada
* 500 - Erro interno no servidor
